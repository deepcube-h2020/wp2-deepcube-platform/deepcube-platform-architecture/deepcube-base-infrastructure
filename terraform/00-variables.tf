# list of variables

# configure the cluster
variable "NETWORK_PROVIDER_TYPE" {
    default = "ovh" # can be: openstack, ovh
    type = string
    description = "Type of the network provider: can be ovh, openstack. Only OVH allows multiple regions."
}

variable "CLUSTER_REGIONS" {
    default = ["GRA5", "GRA11", "UK1", "GRA7", "SBG5", "DE1", "WAW1", "GRA9", "BHS5"]
    type = set(string)
    description = "List of the regions to deploy the network."
}

variable "BASE_DEFAULT_DNS_SERVERS" {
    default = ["1.1.1.1","1.0.0.1"]
    type = list(string)
}

variable "CLUSTER_INTERNAL_PRIVATE_NETWORK_CIDR" {
    default = "10.10.0.0/16"
    type = string
}
variable "CLUSTER_INTERNAL_PRIVATE_NETWORK_NAME" {
    default = "network_deepcube_global_inet"
    type = string
}

variable "OVH_NETWORKS_VLAN_ID" {
     description = "Vlan id of OVH operated network"
     type = number
     default = 1200
}